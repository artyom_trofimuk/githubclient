package com.example.linkapp;

import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;

/**
 * Created by ���� on 01.03.2016.
 */
public class JsonParseStartPoint {
    ArrayList<StartPointDropdown> startPointArrayList;
    public void parseData(String json) {
        startPointArrayList = new ArrayList<>();
        JSONObject jsonObject = (JSONObject) JSONValue.parse(json);
        JSONArray jsonArrayRoutes = (JSONArray) jsonObject.get("data");
        for (int i = 0; i < jsonArrayRoutes.size(); i++) {
            JSONObject jsonStartPoint = (JSONObject) jsonArrayRoutes.get(i);
            new StartPointDropdown().setName(jsonStartPoint.get("name").toString());
        }
    }
    public ArrayList<StartPointDropdown> getStartPointArrayList(){
        return startPointArrayList;
    }
}
