package adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.artyom.githubclient.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import model.User;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersHolder>{
    private ArrayList<User> userArrayList;
    private OnPostItemClickListener onPostItemClickListener;
    private Context context;
    public UsersAdapter(ArrayList<User> userArrayList){
        this.userArrayList = userArrayList;
    }

    @NonNull
    @Override
    public UsersAdapter.UsersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.user_item, parent,false);
        context = parent.getContext();
        return new UsersAdapter.UsersHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersHolder usersHolder, int position) {
        usersHolder.itemView.setTag(position);
        if (userArrayList.get(position).getLogin()!=null){
            usersHolder.twUserLogin.setText(userArrayList.get(position).getLogin());
        }
        if (userArrayList.get(position).getId()!=null){
            usersHolder.twId.setText(userArrayList.get(position).getId());
        }
        if (userArrayList.get(position).getUrlAvatar()!= null) {
            Glide
                    .with(context)
                    .load(userArrayList.get(position).getUrlAvatar())
                    .into(usersHolder.imageViewAvatar);
        }
        usersHolder.itemView.setOnClickListener(v->{
            if (onPostItemClickListener!=null){
                onPostItemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userArrayList.size();
    }

    public void setOnPostItemClickListener(OnPostItemClickListener onPostItemClickListener) {
        this.onPostItemClickListener = onPostItemClickListener;
    }

    public static class UsersHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.twUserLogin)
        TextView twUserLogin;

        @Bind(R.id.twId)
        TextView twId;

        @Bind(R.id.imageViewAvatar)
        CircleImageView imageViewAvatar;

        public UsersHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void clear() {
        userArrayList.clear();
        notifyDataSetChanged();
    }

    public interface OnPostItemClickListener{
        void onItemClick(View view,int position);
    }
}
