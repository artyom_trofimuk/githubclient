package helpers;

import android.content.Context;

import net.ServiceApi;

public class InstanceHelper {
    private static Context mContext;
    private static ServiceApi mApiHelper;

    public static void setHelpers(Context context) {
        mContext = context;
        mApiHelper = new ApiHelper(context).getInstance();
    }

    public static void releaseHelpers() {
        mContext = null;
        mApiHelper = null;
    }

    public static ServiceApi getApiHelper() {
        return mApiHelper;
    }
}
