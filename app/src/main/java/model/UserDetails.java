package model;

import com.google.gson.annotations.SerializedName;

import net.response.BaseResponse;

public class UserDetails extends BaseResponse {
    @SerializedName("avatar_url")
    private String urlAvatar;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("company")
    private String company;
    @SerializedName("following")
    private String  countFollowing;
    @SerializedName("followers")
    private String countFollowers;
    @SerializedName("created_at")
    private String dateCreatedAccount;

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCompany() {
        return company;
    }

    public String getCountFollowing() {
        return countFollowing;
    }

    public String getCountFollowers() {
        return countFollowers;
    }

    public String getDateCreatedAccount() {
        return dateCreatedAccount;
    }
}
