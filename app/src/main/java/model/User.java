package model;

import com.google.gson.annotations.SerializedName;

import net.response.BaseResponse;

public class User extends BaseResponse {
    @SerializedName("id")
    private String id;
    @SerializedName("login")
    private String login;
    @SerializedName("avatar_url")
    private String urlAvatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }
}
