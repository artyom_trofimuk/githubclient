package activity;

import android.app.Application;

import helpers.InstanceHelper;

public class GitHubApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        InstanceHelper.setHelpers(this);
    }
}
