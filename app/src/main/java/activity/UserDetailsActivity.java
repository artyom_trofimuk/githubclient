package activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.artyom.githubclient.R;
import net.Tasks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import model.UserDetails;
import rx.Observable;
import rx.Observer;

public class UserDetailsActivity extends AppCompatActivity {
    @Bind(R.id.imageViewAvatarUserDetail)
    CircleImageView imageViewAvatarUserDetail;

    @Bind(R.id.twNameUserDetail)
    TextView twNameUserDetail;

    @Bind(R.id.twCompanyUserDetail)
    TextView twCompanyUserDetail;

    @Bind(R.id.twEmailUserDetail)
    TextView twEmailUserDetail;

    @Bind(R.id.twFollowersUserDetail)
    TextView twFollowersUserDetail;

    @Bind(R.id.twFollowingUserDetail)
    TextView twFollowingUserDetail;

    @Bind(R.id.twDateCreatedUserDetail)
    TextView twDateCreatedUserDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details_activity);
        ButterKnife.bind(this);
   }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intentUserDetail = getIntent();
        if (intentUserDetail.getStringExtra("login")!=null){
            getSingleUser( intentUserDetail.getStringExtra("login"));
        }
    }

    private void getSingleUser(String login){
        try {
            Observable<UserDetails> userDetails = Tasks.getSingleUser(login);
            userDetails.subscribe(new Observer<UserDetails>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {}

                @Override
                public void onNext(UserDetails userDetails) {
                    if (userDetails!=null){
                        runOnUiThread(()->   {
                            if (userDetails.getUrlAvatar()!= null) {
                                Glide
                                        .with(getApplicationContext())
                                        .load(userDetails.getUrlAvatar())
                                        .into(imageViewAvatarUserDetail);
                            }
                            if (userDetails.getName()!=null){
                                twNameUserDetail.setText(userDetails.getName());
                            }else {
                                twNameUserDetail.setText(getString(R.string.not_known));
                            }
                            if (userDetails.getEmail()!=null){
                                twEmailUserDetail.setText(userDetails.getEmail());
                            }else {
                                twEmailUserDetail.setText(getString(R.string.not_known));
                            }
                            if (userDetails.getCompany()!=null){
                                twCompanyUserDetail.setText(userDetails.getCompany());
                            }else {
                                twCompanyUserDetail.setText(getString(R.string.not_known));
                            }
                            if (userDetails.getCountFollowers()!=null){
                                twFollowersUserDetail.setText(userDetails.getCountFollowers());
                            }else {
                                twFollowersUserDetail.setText(getString(R.string.not_known));
                            }
                            if (userDetails.getCountFollowing()!=null){
                                twFollowingUserDetail.setText(userDetails.getCountFollowing());
                            }else {
                                twFollowingUserDetail.setText(getString(R.string.not_known));
                            }
                            if (userDetails.getDateCreatedAccount()!=null){

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                try {
                                    Date date = dateFormat.parse(userDetails.getDateCreatedAccount());
                                    twDateCreatedUserDetail.setText(date.toString());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }else {
                                twDateCreatedUserDetail.setText(getString(R.string.not_known));
                            }
                        });
                    }
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
