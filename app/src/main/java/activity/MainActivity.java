package activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import com.example.artyom.githubclient.R;
import net.Tasks;
import java.util.ArrayList;
import adapters.UsersAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import model.User;
import rx.Observable;
import rx.Observer;


public class MainActivity extends AppCompatActivity {
    @Bind(R.id.usersRecyclerView)
    RecyclerView usersRecyclerView;

    @Bind(R.id.progressBarUsers)
    ProgressBar progressBarUsers;

    @Bind(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    private UsersAdapter usersAdapter;
    private  LinearLayoutManager mLayoutManager;
    private ArrayList<User> userArrayList;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem;
    int visibleItemCount;
    int totalItemCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        progressBarUsers.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mLayoutManager = new LinearLayoutManager(this);
        usersRecyclerView.setLayoutManager(mLayoutManager);
        userArrayList = new ArrayList<>();

        usersAdapter = new UsersAdapter(userArrayList);
        usersRecyclerView.setAdapter(usersAdapter);
        getUsers();

        usersAdapter.setOnPostItemClickListener((view, position) -> {
            Intent intent = new Intent(MainActivity.this,UserDetailsActivity.class);
            if (userArrayList.get(position).getLogin()!=null){
                intent.putExtra("login",userArrayList.get(position).getLogin());
                startActivity(intent);
            }
        });

        addPullToRefresh();
        addPagination();
    }

    private void getUsers(){
        try {
            Observable<ArrayList<User>>  usersResponseObservable = Tasks.getUsers();
            usersResponseObservable.subscribe(new Observer<ArrayList<User>> () {
                @Override
                public void onCompleted() {
                    progressBarUsers.setVisibility(View.INVISIBLE);
                    runOnUiThread(()-> usersAdapter.notifyDataSetChanged());
                }

                @Override
                public void onError(Throwable e) {}

                @Override
                public void onNext(ArrayList<User> users) {
                    if (users!=null){
                        userArrayList.addAll(users);
                    }
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void addPagination(){
        usersRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }

                if (!loading && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    getUsers();
                    loading = true;
                }
            }
        });
    }

    public void addPullToRefresh(){
        swipeContainer.setOnRefreshListener(() -> {
            usersAdapter.clear();
            getUsers();
            swipeContainer.setRefreshing(false);
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright);
    }
}
