package net;

import java.util.ArrayList;

import model.User;
import model.UserDetails;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface ServiceApi {
    String ROOT = "https://api.github.com";

    @GET("/users")
    Observable<ArrayList<User>> getUsers();

    @GET("users/{login}")
    Observable<UserDetails> getSingleUser(@Path("login") String login);
}
