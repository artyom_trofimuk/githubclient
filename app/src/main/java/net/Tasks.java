package net;

import java.util.ArrayList;

import helpers.InstanceHelper;
import model.User;
import model.UserDetails;
import rx.Observable;

public class Tasks {
    public static Observable<ArrayList<User>>  getUsers(){
        return InstanceHelper.getApiHelper().getUsers().retry(3);
    }

    public static Observable<UserDetails> getSingleUser(String login){
        return InstanceHelper.getApiHelper().getSingleUser(login).retry(3);
    }
}
